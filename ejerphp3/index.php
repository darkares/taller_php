<!doctype html>
<html>
    <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <title>Ejercicio 3</title>
        <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/css/bootstrap.min.css" integrity="sha384-MCw98/SFnGE8fJT3GXwEOngsV7Zt27NXFoaoApmYm81iuXoPkFOJwJ8ERdknLPMO" crossorigin="anonymous">
		<link href="https://fonts.googleapis.com/css?family=Roboto|Roboto+Mono" rel="stylesheet">
    </head>
    <body>
        <div class="container text-center mb-3">
        	<h1 class="h1">Ejercicio número 3</h1>
        </div>
        <div class="container">
        	<div class="row">
        		<div class="col-12 text-center">
        			<p>Leer un número del 1 al 7 e imprimir el día al que corresponde.</p>
        		</div>
        	</div>
        </div>
        <section>
			<div class="row">
				<div class="col-12">
		        	<form action="" method="get" name="frmDia">
			        	<div class="row">
			        		<div class="col-12 col-md-6 text-right">
			        			<label for="nmbDia">Ingrese el dìa de la semana que desea ver :</label>
			        		</div>
			        		<div class="col-12 col-md-6">
			        			<input type="number" min="1" max="7" id="nmbDia" name="nmbDia">
			        		</div>
			        	</div>
			        	<?php 
			        		function dia()
			        		{
				        		if (!empty($_GET['nmbDia'])) {
				        			$dia = $_GET['nmbDia'];
				        			switch ($dia){
				        				case 1:
				        					$txt = "Lunes";
				        					break;
				        				case 2:
				        					$txt = "Martes";
				        					break;

				        				case 3:
				        					$txt = "Miercoles";
				        					break;
				        				case 4:
				        					$txt = "Jueves";
				        					break;
				        				case 5:
				        					$txt = "Viernes";
				        					break;

				        				case 6:
				        					$txt = "Sabado";
				        					break;

				        				case 7:
				        					$txt = "Domingo";
				        					break;
				        				default:
				        					$txt = "Dia no valido";
				        					break;
				        			}
				        		}else{
				        			$txt =  'Dia no valido';
				        		}
				        		return $txt;
			        		}
			        	?>
			        	<div class="row">
			        		<div class="col-12 col-md-6 text-right">
			        			<label> El día de la semana correpondiente es :</label>
			        		</div>
			        		<div class="col-12 col-md-6" id="dia">
			        			<?php echo dia(); ?>
			        		</div>
			        	</div>
			        	<div class="row">
							<div class="col-12 text-center">
								<input type="submit" value="Mostrar día" class="btn btn-dark">
							</div>
						</div>
					</form>
				</div>
			</div>
        </section>


        <script src="https://code.jquery.com/jquery-3.3.1.slim.min.js" integrity="sha384-q8i/X+965DzO0rT7abK41JStQIAqVgRVzpbzo5smXKp4YfRvH+8abtTE1Pi6jizo" crossorigin="anonymous"></script>
		<script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.3/umd/popper.min.js" integrity="sha384-ZMP7rVo3mIykV+2+9J3UJ46jBk0WLaUAdn689aCwoqbBJiSnjAK/l8WvCWPIPm49" crossorigin="anonymous"></script>
		<script src="https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/js/bootstrap.min.js" integrity="sha384-ChfqqxuZUCnJSK3+MXmPNIyE6ZbWh2IMqE241rYiqJxyMiZ6OW/JmZQ5stwEULTy" crossorigin="anonymous"></script>
    </body>
</html>